package com.myprojects.esales.dto;

import java.util.UUID;

public class SalesDTO {
	
	private UUID id;
	private Integer tomatoes;
	private String provider;
	private Long timestamp;
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID uuid) {
		this.id = uuid;
	}
	public Integer getTomatoes() {
		return tomatoes;
	}
	public void setTomatoes(Integer tomatoes) {
		this.tomatoes = tomatoes;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	
}
