package com.myprojects.esales.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class SalesResourceExceptionHandler {

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Argument 'size' must be an integer")
	public ResponseEntity<String> handleAccessingWrongArguments(MethodArgumentTypeMismatchException ex) {
		return new ResponseEntity<String>("Argument 'size' must be an integer " + ex.getMessage(), HttpStatus.BAD_REQUEST);
	}

}