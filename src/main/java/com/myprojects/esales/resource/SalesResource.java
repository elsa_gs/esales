package com.myprojects.esales.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.myprojects.esales.service.SalesService;
import com.myprojects.esales.dto.SalesDTO;

/*
 * REST ENDPOINTS:
 * 	GET  	- /data
 *  GET 	- /data?size={size}
 */

@RestController
@RequestMapping("/data")
public class SalesResource {

	@Autowired
	private SalesService salesService;

	/**
	 * Rest end point to get info on tomato sales
	 * @param size is optional
	 * @return list of sales
	 */
	@GetMapping
	@ResponseBody
	public List<SalesDTO> get(@RequestParam(value="size", required=false) Integer size) {
		return salesService.getSales(size);
	}

}
