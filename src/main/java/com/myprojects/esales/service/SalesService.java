package com.myprojects.esales.service;

import java.util.List;

import com.myprojects.esales.dto.SalesDTO;

public interface SalesService {

	/**
	 * Get information on tomato sales
	 */
	public List<SalesDTO> getSales(Integer size);

}
