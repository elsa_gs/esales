package com.myprojects.esales.serviceImpl;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.myprojects.esales.service.SalesService;
import com.myprojects.esales.dto.SalesDTO;

@Service
public class SalesServiceImpl implements SalesService {

	private static Log log = LogFactory.getLog(SalesServiceImpl.class);

	public static final String[] PROVIDERS = {"Heinz", "Hunt's", "Del Monte", "Le Ol' Granma"};
	public static final Long BEGIN_TIME = Timestamp.valueOf("2017-01-01 00:00:00").getTime();
	public static final Integer DEFAULT_SIZE = 3;
	public static final Integer MIN = 1;
	public static final Integer MAX = 2000;

	public List<SalesDTO> getSales(Integer size) {

		log.info("Returning sales...");

		List<SalesDTO> result = new ArrayList<SalesDTO>();

		// Initialize size to 3 if it is empty
		if(size == null){
			size = DEFAULT_SIZE;
		}

		log.info("Generating " + size + " result(s)...");

		for(int i = 0; i < size ; i++){
			result.add(generateItem());
		}

		// Order list by timestamp asc
		result.sort(new Comparator<SalesDTO>() {
			public int compare(SalesDTO o1, SalesDTO o2) {
				return (o1.getTimestamp().compareTo(o2.getTimestamp()));
			}
		});

		return result;

	}

	private SalesDTO generateItem(){
		SalesDTO dto = new SalesDTO();
		dto.setId(UUID.randomUUID());
		dto.setTomatoes(generateRandomInteger(MIN, MAX));
		dto.setProvider(getRandomProvider());
		dto.setTimestamp(generateRandomTimestamp());

		return dto;
	}

	protected Integer generateRandomInteger(Integer min, Integer max){
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	protected String getRandomProvider() {
		return PROVIDERS[generateRandomInteger(0, PROVIDERS.length - 1)];
	}

	protected Long generateRandomTimestamp() {
		Instant instant = Instant.now();
		long endTime = instant.toEpochMilli();

		long diff = endTime - BEGIN_TIME + 1;
		return BEGIN_TIME + (long) (Math.random() * diff);
	}
}
