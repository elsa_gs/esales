var app = angular.module('app', ['ngRoute']);
app.config(function($routeProvider, $locationProvider){
	
	$locationProvider.html5Mode(true).hashPrefix('!');
	
    $routeProvider
        .when('/',{
            templateUrl: '/app/views/salesData.html',
            controller: 'salesDataController'
        })
        .otherwise({
        	redirectTo: '/'
        });

});