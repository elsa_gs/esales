angular.module('app').controller('salesDataController', function($scope, $http) {

	$scope.hideTable = true;
	$scope.hideErrorMsg = true;
	$scope.data = [];
	$scope.invalidArgumentErrorMsg = '';

	$scope.retrieveSales = function(){
		var config = {
			params: { size: $scope.size }
		};

		$http.get("/data", config).then(function(response) {
			$scope.data = response.data;
			$scope.hideTable = false;
			$scope.hideErrorMsg = true;
		},
		function(error) {
			$scope.invalidArgumentErrorMsg = 'Something went wrong: ' + error.data.message; 
			$scope.hideErrorMsg = false;
			$scope.hideTable = true;
		});
	};

});