package com.myprojects.esales.resource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SalesResourceTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	public int PORT = 8080;

	@Test
	@SuppressWarnings("unchecked")
	public void getSalesEmptySize(){
		assertThat(this.restTemplate.getForObject("http://localhost:" + PORT + "/data",
				List.class)).hasSize(3);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void getSalesWithSize(){
		assertThat(this.restTemplate.getForObject("http://localhost:" + PORT + "/data?size=5",
				List.class)).hasSize(5);
	}

	@Test
	public void getSalesWithInvalidArgument(){
		ResponseEntity<String> response = this.restTemplate.getForEntity("http://localhost:" + PORT + "/data?size=wrong",
				String.class);
		assertNotNull(response);
		assertThat(response.getStatusCode().compareTo(HttpStatus.BAD_REQUEST));
		assertTrue(response.getBody().contains("Argument 'size' must be an integer"));
	}
}
