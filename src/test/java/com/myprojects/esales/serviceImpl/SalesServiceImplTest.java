package com.myprojects.esales.serviceImpl;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.myprojects.esales.dto.SalesDTO;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SalesServiceImplTest {

	@Autowired
	private SalesServiceImpl service;

	@Test
	public void getSalesWhenSizeEmpty(){
		List<SalesDTO> result = service.getSales(null);
		assertNotNull(result);
		assertTrue(result.size() == SalesServiceImpl.DEFAULT_SIZE);
	}

	@Test
	public void getSalesWhenSizePopulated(){
		Integer size = 5;
		List<SalesDTO> result = service.getSales(size);
		assertNotNull(result);
		assertTrue(result.size() == size);
	}

	@Test
	public void getSalesWithOrder(){
		Integer size = 2;
		List<SalesDTO> result = service.getSales(size);

		assertNotNull(result);
		assertEquals(-1, result.get(0).getTimestamp().compareTo(result.get(size - 1).getTimestamp()));
	}

	@Test
	public void generateRandomInteger(){
		Integer result = service.generateRandomInteger(SalesServiceImpl.MIN, SalesServiceImpl.MAX);

		assertNotNull(result);
		assertTrue(SalesServiceImpl.MIN <= result);
		assertTrue(SalesServiceImpl.MAX >= result);
	}

	@Test
	public void getRandomProvider(){
		List<String> providersAsList = Arrays.asList(SalesServiceImpl.PROVIDERS);

		String result = service.getRandomProvider();

		assertNotNull(result);
		assertTrue(providersAsList.contains(result));
	}

	@Test
	public void getRandomTimestamp(){
		long result = service.generateRandomTimestamp();

		Instant instant = Instant.now();
		long now = instant.toEpochMilli();

		assertNotNull(result);
		assertTrue(SalesServiceImpl.BEGIN_TIME <= result);
		assertTrue(now >= result);
	}

}
