# Tomato Sales

This application queries the latest tomato sales that happened on the biggest tomato market.

### Instructions

* Checkout the project
* Compile the project from command line          
`mvn clean install`
* Run the application
	* From command line
	`mvn spring-boot:run`
	* From Eclipse
        * Go to src/main/java/com/myprojects/esales/Application.java
        * Right click and select "Run As - Java Application"
* Open your browser and type url
`http://localhost:8080`