module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
	jshint: {
	  // files to check
	  files: ['Gruntfile.js', 'src/main/resources/static/app/scripts/*.js'],
	  options: {
	    globals: {
	      jQuery: true,
	      console: true,
	      module: true
	    }
	  }
	},
	wiredep: {
      task: {
        src: 'src/main/resources/static/index.html'
      }
    }
  });

  // Load the plugin that provides the "jshint" and "wiredep" tasks
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-wiredep');

  // Default task(s).
  grunt.registerTask('default', ['jshint', 'wiredep']);

};